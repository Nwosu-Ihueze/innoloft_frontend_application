import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import store from './redux/store/index';

let WithStore = () => <Provider store={store}><App /> </Provider>

ReactDOM.render(
  <WithStore />,
  document.getElementById('root')
);


