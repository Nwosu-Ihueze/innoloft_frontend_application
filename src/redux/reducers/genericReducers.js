import { SHOW_ACCOUNT_SETTING, SHOW_USER_INFO, SHOW_SIDEBAR, SHOW_ALERT } from '../actions/types.js';

let sidebarStat = window.innerWidth > 578 ? true : false

let initialState = {
    show_account_setting: true,
    show_user_info: false,
    show_sidebar: sidebarStat,
    show_alert: false
}

export default function(state = initialState, action) {
    switch (action.type) {
        case SHOW_ACCOUNT_SETTING:
            return {
                ...state,
                show_account_setting: action.payload,
            }
        case SHOW_USER_INFO:
            return {
                ...state,
                show_user_info: action.payload,
            }
        case SHOW_SIDEBAR:
            return {
                ...state,
                show_sidebar: action.payload,
            }
        case SHOW_ALERT:
            return {
                ...state,
                show_alert: action.payload,
            }
        default:
            return state;
    }
}