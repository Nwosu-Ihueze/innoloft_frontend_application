import { combineReducers } from 'redux'
import genericReducers from './genericReducers'


export default combineReducers({
    generic: genericReducers,
})