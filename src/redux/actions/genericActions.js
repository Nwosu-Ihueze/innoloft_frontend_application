import { SHOW_USER_INFO, SHOW_ACCOUNT_SETTING, SHOW_SIDEBAR, SHOW_ALERT } from './types';


export const showUserInfo = data => dispatch =>  {  
    // this handles the change of the userInfo state in the redux store
    dispatch ({
        type: SHOW_USER_INFO,
        payload: data
    })
}

export const showAccountSetting = data => dispatch =>  {  
    // this handles the change of the showLogin state in the redux store
    dispatch ({
        type: SHOW_ACCOUNT_SETTING,
        payload: data
    })
}

export const showSidebar = data => dispatch =>  {  
    // this handles the change of the showSidebarstate in the redux store
    dispatch ({
        type: SHOW_SIDEBAR,
        payload: data
    })
}

export const submitAction = data => dispatch =>  {  
    // this handles the change of the showSidebarstate in the redux store
    fetch('http://example.com/clark.json')
        .then((result) => {})
        .catch((err) => {
         //this is a demo api so it would definitly fail   
         dispatch ({
            type: SHOW_ALERT,
            payload: true
        })
    });
}

export const showAlert = data => dispatch =>  {  
    // this handles the change of the showSidebarstate in the redux store
    dispatch ({
        type: SHOW_ALERT,
        payload: data
    })
}