import React from 'react';
import Main from "./components/main";
import Aside from './components/Aside'
import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "./styles/global";
import { lightTheme } from "./styles/theme";
import './styles/App.css'
import { showSidebar } from './redux/actions/genericActions'
import { connect } from 'react-redux'
import Sidebar from './components/Aside';


const App = ({ showSidebar, show_sidebar }) => {
  window.addEventListener("resize", function(){
    let sidebarStat = window.innerWidth > 578 ? true : false
    showSidebar(sidebarStat)
  });

  return (
    <ThemeProvider theme= {lightTheme }>
      <GlobalStyles />
      <div className="app" >
        { show_sidebar && <Aside /> }
        <Main />
      </div>
      
    </ThemeProvider>
  
  );
}

function mapStateToProps(store) {
  return {
      show_sidebar: store.generic.show_sidebar,
  }
}

export default connect(mapStateToProps, { showSidebar })(App)