// Signup form validation
export const accountSettingFormvalidate = values => {
    let specialChar = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const errors = {};

    switch(true) {
        case !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email):
          errors.email = 'provide a valid email'
          break;
        case values.password1 && !values.password2:
            errors.password1 = 'provide both password fields'
          break;
        case !values.password1 && values.password2:
          errors.password2 = 'provide both password fields'
          break
        case values.password1 !== values.password2:
            errors.password2 = 'your password did not match' 
        break;
        
      }

    return errors;
}