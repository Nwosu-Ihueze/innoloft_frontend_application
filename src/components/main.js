import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import UserInputTabs from './UserInputTabs'
import styled from 'styled-components'
import Header from './Header'
import '../styles/main.css'
import User from './User'
import UserInfo from './userInfo'
import Alert from './Alert'


const Main = ({ show_account_setting, show_user_info, show_alert }) => {

    return (
        <div className="main">
            <Header />
           <div className="main__content">
                <UserInputTabs />
                { show_alert && <Alert /> }
                { show_account_setting && <User /> }
                { show_user_info && <UserInfo /> }
           </div> 
        </div>
    )
}

function mapStateToProps(store) {
    return {
        show_account_setting: store.generic.show_account_setting,
        show_user_info: store.generic.show_user_info,
        show_alert: store.generic.show_alert
    }
}
 
export default connect(mapStateToProps, {})(Main)