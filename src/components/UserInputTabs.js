import React from 'react';
import { connect } from 'react-redux'
import { showUserInfo, showAccountSetting } from '../redux/actions/genericActions'
import '../styles/UserInputTabs.css'

const UserInputTabs = ({ show_user_info, show_account_setting, showUserInfo, showAccountSetting }) => {
    const handleShowUserInfo = () => {
        showUserInfo(true)
        showAccountSetting(false)
    }

    const handleShowAccountSettings = () => {
        showUserInfo(false)
        showAccountSetting(true)
    }

    return ( 
    <div className="userInputTab">
        <p className="accountSetting" onClick={handleShowAccountSettings} >Account Setting</p>
        <p className="userInfo" onClick={handleShowUserInfo} >User Information</p>
    </div> 
    );
}
 
    function mapStateToProps(store) {
        return {
            show_account_setting: store.generic.show_account_setting,
            show_user_info: store.generic.show_user_info
        }
    }
 
export default connect(mapStateToProps, { showUserInfo, showAccountSetting })(UserInputTabs)