import React from 'react'
import styled from 'styled-components'
import '../styles/Alert.css'
import { connect } from 'react-redux'
import { showAlert } from '../redux/actions/genericActions'


const Alert = ({ showAlert }) => {

    return (
        <div className="alert">
            <p>Your data has been saved. thank you.</p>
            <h5 onClick={() => showAlert(false)} >x</h5>
        </div>
    )
}


export default connect(null, { showAlert })(Alert)