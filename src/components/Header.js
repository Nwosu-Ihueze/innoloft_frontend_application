import React from 'react'
import styled from 'styled-components'
import '../styles/header.css'
import { showSidebar } from '../redux/actions/genericActions'
import { connect } from 'react-redux'
import MenuIcon from '@material-ui/icons/Menu'


const Header = ({ showSidebar, show_sidebar }) => {

    return (
        <div className="header">
            <MenuIcon onClick={() => showSidebar(!show_sidebar)} />
            <h3 className="header-brand">Fortress of Solitude</h3>
        </div>
    )
}


function mapStateToProps(store) {
    return {
        show_sidebar: store.generic.show_sidebar,
    }
  }
  
export default connect(mapStateToProps, { showSidebar })(Header)