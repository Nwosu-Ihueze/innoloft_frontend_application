import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { accountSettingFormvalidate } from '../utils/formValidtion'
import { useFormik } from 'formik';
import { submitAction } from '../redux/actions/genericActions'
import '../styles/User.css';


const UserInfo = ({ submitAction }) => {
    const formik = useFormik({
        initialValues: {
          firstname: 'Clark',
          lastname: 'Kent',
          address: 'Planet Crypton',
          country: 'Austria'
        },

        onSubmit: values => {
            submitAction(values)
        },
    });

    return ( 
        <div className="login">
            <i> <h3> Update Your Information </h3> </i>

            <div className="login__box">
                <form onSubmit={formik.handleSubmit} autoComplete="off">
                    <div className="input__wrap">
                        <label>First Name:</label>
                        <input 
                            id="firstname"
                            name="firstname" 
                            onChange={formik.handleChange} 
                            value={formik.values.firstname}
                            type="text" 
                        />
                        { formik.errors.firstname ? <div className="error">{ formik.errors.firstname }</div> : null }
                    </div>
                    <div className="input__wrap">
                        <label>Last Name:</label>
                        <input 
                            id="lasttname"
                            name="lasttname" 
                            onChange={formik.handleChange} 
                            value={formik.values.lastname}
                            type="text" 
                        />
                        { formik.errors.lastname ? <div className="error">{ formik.errors.lastname }</div> : null }
                    </div>
                    <div className="input__wrap">
                        <label htmlFor="password1" >Address:</label>
                        <input 
                            id="address"
                            name="address" 
                            onChange={formik.handleChange}
                            value={formik.values.address} 
                            type="text" 
                        />
                    </div>
                    <div className="input__wrap">
                        <label htmlFor="password1" >Earth country:</label>
                        <select 
                            id="country"
                            name="country" 
                            onChange={formik.handleChange}
                            value={formik.values.country} 
                        >
                            <option value="Germany">Germany</option>
                            <option value="Switzaland">Switzaland</option>
                            <option selected value="Austria">Austria</option>
                        </select>
                    </div>

                    <div className="input__wrap">
                        <button type="submit" className="submit__btn"> Update Info </button>
                    </div>

                </form>
            </div>

        </div>
     );
}

function mapStateToProps(store) {
    return {
        show_login: store.generic.show_login,
        show_user_info: store.generic.show_user_info
    }
}
 
export default connect(mapStateToProps, { submitAction })(UserInfo)