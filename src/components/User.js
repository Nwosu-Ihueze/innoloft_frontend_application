import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { accountSettingFormvalidate } from '../utils/formValidtion'
import { useFormik } from 'formik';
import PasswordStrengthBar from 'react-password-strength-bar';
import { submitAction } from '../redux/actions/genericActions'
import '../styles/User.css';


const Login = ({ submitAction }) => {
    const formik = useFormik({
        initialValues: {
          email: 'clarkkent@email.com',
          password1: 'password123',
          password2: 'password123',
        },
        validate: accountSettingFormvalidate,

        onSubmit: values => {
            submitAction(values)
        },
    });

    return ( 
        <div className="login">
            <i> <h3> Your Account Setting </h3> </i>

            <div className="login__box">
                <form onSubmit={formik.handleSubmit} autoComplete="off">
                    <div className="input__wrap">
                        <label>New Email:</label>
                        <input 
                            id="email"
                            name="email" 
                            onChange={formik.handleChange} 
                            value={formik.values.email}
                            type="text" 
                        />
                        { formik.errors.email ? <div className="error">{ formik.errors.email }</div> : null }
                    </div>
                    <div className="input__wrap">
                        <label htmlFor="password1" >Password:</label>
                        <input 
                            id="password1"
                            name="password1" 
                            onChange={formik.handleChange}
                            value={formik.values.password1} 
                            type="password" 
                        />
                        <PasswordStrengthBar password={formik.values.password1} />
                        { formik.errors.password1 ? <div className="error">{ formik.errors.password1 }</div> : null }
                    </div>
                    <div className="input__wrap">
                        <label htmlFor="password1" >Confirm Password:</label>
                        <input 
                            id="password2"
                            name="password2" 
                            onChange={formik.handleChange}
                            value={formik.values.password2} 
                            type="password" 
                        />
                        <PasswordStrengthBar password={formik.values.password2} />
                        { formik.errors.password2 ? <div className="error">{ formik.errors.password2 }</div> : null }
                    </div>
                    <div className="input__wrap">
                        <button type="submit" className="submit__btn"> Update Account </button>
                    </div>

                </form>
            </div>

        </div>
     );
}

export default connect(null, { submitAction })(Login)